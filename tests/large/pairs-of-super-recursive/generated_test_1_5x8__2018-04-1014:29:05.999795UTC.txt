A: rec x . {B!d;{B!e;{B?e;x, B?a;x}, B!g;{B?a;{B!b;{B?e;x}}}}}
B: rec x . {A?d;{A?e;{A!e;x, A!a;x}, A?g;{A!a;{A?b;{A!e;x}}}}}